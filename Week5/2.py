import matplotlib.pyplot as plt
import numpy as np

def MH(n):
    return 1+np.cos(n)


def PH(n):
    n = (n + 2 * pi) % (2 * pi)
    return -n*(n <= pi)+(-n+2*pi)*(n > pi)


def single(w, n):
    return 5*np.cos(w*pi*n)


def output(w, n):
    inputSingle = single(w, n)
    inputSingleR1 = np.roll(inputSingle, 1)
    inputSingleR1[0:1] = 0
    inputSingleR2 = np.roll(inputSingle, 2)
    inputSingleR2[0:2] = 0
    return 0.5*inputSingle+inputSingleR1+0.5*inputSingleR2

pi = np.pi
n1 = np.arange(0, 2*pi, 2*pi/40)
n2 = np.arange(0, 8*pi, 2*pi/40)
n3 = np.arange(-4*pi, 4*pi, 2*pi/40)
x1 = np.arange(0, 32)
x2 = x1

plt.figure(1)
plt.subplot(3, 1, 1)
plt.stem(n1, PH(n1))
plt.subplot(3, 1, 2)
plt.stem(n2, PH(n2))
plt.subplot(3, 1, 3)
plt.stem(n3, PH(n3))

plt.figure(2)
plt.subplot(3, 1, 1)
plt.stem(n1, MH(n1))
plt.subplot(3, 1, 2)
plt.stem(n2, MH(n2))
plt.subplot(3, 1, 3)
plt.stem(n3, MH(n3))

plt.figure(3)
plt.subplot(2, 1, 1)
plt.stem(x1, single(0.25, x1))
plt.subplot(2, 1, 2)
plt.stem(x2, single(0.75, x2))

plt.figure(4)
plt.subplot(2, 1, 1)
plt.stem(x1, output(0.25, x1))
plt.subplot(2, 1, 2)
plt.stem(x2, output(0.75, x2))

plt.show()
