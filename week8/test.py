import numpy as np

arr = np.arange(9)
print(arr**2)
print(np.power(arr, 2))
mat = arr.reshape(3, 3)
print(mat**2)
print(np.power(mat, 2))