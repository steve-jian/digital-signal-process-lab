import numpy as np
import scipy.integrate

def avaValue(data):
    data = np.array(data)
    size = np.size(data)
    total = np.sum(data)
    return total/size

def variance(data,ava,size,total=0):
    data = np.array(data)
    flag = np.size(data)
    if flag != 0:
        x = data[-1]
        total += ((x-ava))**2
        data = data[:-1]
        return variance(data,ava,size,total)
    return total/size
